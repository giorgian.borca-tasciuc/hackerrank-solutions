#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <cmath>
#include <cstring>


unsigned abs_diff(const unsigned a, const unsigned b);

constexpr size_t max_length {100000};
unsigned dp[max_length][2] {};
int main(void) {
    unsigned ntestcases;
    std::cin >> ntestcases;

    for (size_t i {0}; i < ntestcases; ++i) {
        size_t size;
        std::cin >> size;
        std::vector<unsigned> b(size);
        for (size_t j = 0; j < size; ++j)
            std::cin >> b[j];

        dp[0][0] = 0;
        dp[0][1] = 0;
        for (size_t j {1}; j < size; ++j) {
            dp[j][0] = std::max(
                    dp[j - 1][0], 
                    dp[j - 1][1] + abs_diff(1, b[j - 1])
                    );
            dp[j][1] = std::max(
                    dp[j - 1][0] + abs_diff(1, b[j]),
                    dp[j - 1][1] + abs_diff(b[j], b[j - 1])
                    );

        }
        std::cout << std::max(dp[size - 1][0], dp[size - 1][1]) << '\n';

        memset(dp, 0, sizeof(dp));
    }

    return 0;
}

unsigned abs_diff(const unsigned a, const unsigned b) {
    return (a <= b) ? b - a : a - b;
}
