#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

int main(void) {
    std::vector<unsigned> coins {};
    std::vector<unsigned long long> ways {};

    unsigned amount, ncoins;
    std::cin >> amount;
    ways.resize(amount + 1);

    std::cin >> ncoins;

    unsigned coin;
    for (size_t i {0}; i < ncoins; ++i) {
        std::cin >> coin;
        coins.push_back(coin);
    }

    std::sort(std::begin(coins), std::end(coins));

    ways[0] = 1;
    for (size_t i = 0; i < ncoins; ++i)
        for (size_t j = 0; j < amount; ++j)
            if (j + coins[i] <= amount)
                ways[j + coins[i]] += ways[j];

    std::cout << ways[amount] << '\n';

    return 0;
}
