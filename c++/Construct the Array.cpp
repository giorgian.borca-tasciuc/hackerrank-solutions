#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

constexpr unsigned max_length {100000};

/* 
 * dp[n][0] is the number of sequences of length n that end in 1
 * dp[n][1] is the number of sequences of length n that end in x
 * dp[n][2] is the number of sequences of length n that end in all else
 */
unsigned long long dp[max_length][3];
constexpr unsigned long long m {static_cast<unsigned long long>(1e9) + 7};

int main(void) {
    unsigned n, k, x;
    std::cin >> n >> k >> x;

    dp[0][0] = 1;
    dp[0][1] = 0;
    dp[0][2] = 0;
    if (x == 1) {
        for (size_t i {0}; i < n - 1; ++i) {
            dp[i + 1][0] = dp[i][2];
            dp[i + 1][2] = ((k - 1) * dp[i][0]) % m + ((k - 2) * dp[i][2]) % m;
            dp[i + 1][2] %= m;
        }

        std::cout << dp[n - 1][0] << '\n';
    } else {
        for (size_t i {0}; i < n - 1; ++i)  {
            dp[i + 1][0] = (dp[i][1] + dp[i][2]) % m;
            dp[i + 1][1] = (dp[i][0] + dp[i][2]) % m;
            dp[i + 1][2] = ((k - 2)*((dp[i][0] + dp[i][1]) % m)) % m + ((k - 3)*dp[i][2]) % m;
            dp[i + 1][2] %= m;
        }
        std::cout << dp[n - 1][1] << '\n';
    }

    return 0;
}
