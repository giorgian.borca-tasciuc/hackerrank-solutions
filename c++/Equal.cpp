#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <cmath>
#include <cstring>
#include <cassert>
#include <numeric>
#include <tuple>
#include <set>

int main(void) {
    unsigned ntestcases;
    std::cin >> ntestcases;

    for (size_t i {0}; i < ntestcases; ++i) {
        size_t size;
        std::cin >> size;
        std::vector<int> b(size);
        for (size_t j = 0; j < size; ++j)
            std::cin >> b[j];

        sort(begin(b), end(b));

        int min_cost {0};
        for (size_t k {0}; k < size; ++k) {
            const int c {std::abs(b[k] - b[0])};
            min_cost += c / 5 + (c % 5) / 2 + (c % 5) % 2;
        }

        for (int min {b[0] - 4}; min < b[0]; ++min) {
            int cost {0};
            for (size_t k {0}; k < size; ++k) {
                const int c {std::abs(b[k] - min)};
                cost += c / 5 + (c % 5) / 2 + (c % 5) % 2;
            }

            min_cost = std::min(cost, min_cost);
        }

        std::cout << min_cost << '\n';
    }

    return 0;
}
